import React from 'react'
import {Link} from "react-router-dom";

export const LinksList = ({links}) => {
    if (!links.length) {
        return <p className="center">No links added</p>
    }

    return (
        <table>
            <thead>
            <tr>
                <th>№</th>
                <th>Original</th>
                <th>Short</th>
                <th />
            </tr>
            </thead>

            <tbody>
            {links.map(({from, to, _id}, index) => {
                return (
                    <tr key={_id}>
                        <td>{index + 1}</td>
                        <td>{from}</td>
                        <td>{to}</td>
                        <td>
                            <Link to={`/detail/${_id}`} >Open</Link>
                        </td>
                    </tr>
                )
            })}

            </tbody>
        </table>
    )
}