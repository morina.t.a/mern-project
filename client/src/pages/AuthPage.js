import React, {useContext, useEffect, useState} from 'react'
import {useHttp} from '../hooks/http.hook'
import {useMessage} from "../hooks/message.hook";
import {AuthContext} from "../context/AuthContext";

export const AuthView = () => {
  const auth = useContext(AuthContext)
  const message = useMessage()
  const {loading, error, request, clearError} = useHttp()
  const [form, setForm] = useState({
    email: '', password: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  useEffect(() => {
    window.M.updateTextFields()
  }, [])

  const handleChange = ({target: {name, value}}) => {
    setForm({ ...form, [name]: value })
  }

  const handleSignUp = async () => {
    try {
      const data = await request('/api/auth/register', 'POST', {...form})
      message(data.message)
    } catch (error) {}
  }

  const handleSignIn = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', {...form})
      auth.login(data.token, data.userId)
    } catch (error) {}
  }

  return (
    <div className="row" style={{paddingTop: 100}}>
      <div className="col s6 offset-s3">
          <div className="card blue darken-1">
            <div className="card-content white-text">
              <span className="card-title">Authorization</span>
              <div>
                <div className="input-field">
                  <input
                    placeholder="Enter email"
                    id="email"
                    type="text"
                    name="email"
                    className="yellow-input"
                    value={form.email}
                    onChange={handleChange}
                  />
                  <label htmlFor="email">Email</label>
                </div>
                <div className="input-field">
                  <input
                      placeholder="Enter password"
                      id="password"
                      type="password"
                      name="password"
                      className="yellow-input"
                      value={form.password}
                      onChange={handleChange}
                  />
                  <label htmlFor="password">Password</label>
                </div>
              </div>
            </div>
            <div className="card-action">
              <button
                className="btn yellow darken-4"
                style={{marginRight: 10}}
                onClick={handleSignIn}
              >
                Sign In
              </button>
              <button
                className="btn grey lighten-1 black-text"
                onClick={handleSignUp}
                disabled={loading}
              >
                Sign Up
              </button>
            </div>
          </div>
      </div>
    </div>
  )
}
