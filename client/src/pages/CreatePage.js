import React, {useContext, useEffect, useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {useHttp} from "../hooks/http.hook";
import {AuthContext} from "../context/AuthContext";

export const CreateView = () => {
    const navigate = useNavigate()
    const auth = useContext(AuthContext)
    const {request} = useHttp()
    const [link, setLink] = useState()

    useEffect(() => {
        window.M.updateTextFields()
    }, [])

    const handleChange = ({target: {value}}) => {
        setLink(value)
    }

    const handleKeyPress = async ({key}) => {
        if (key === 'Enter') {
            try {
                const data = await request('/api/link/generate', 'POST', {from: link}, {
                    Authorization: `Bearer ${auth.token}`
                })
                navigate(`/detail/${data.link._id}`)
            } catch (e) {}
        }
    }

    return (
        <div className="row">
            <div className="col s8 offset-s2" style={{paddingTop: '2rem'}}>
                <div className="input-field">
                    <input
                        placeholder="Enter link"
                        id="link"
                        type="text"
                        value={link || ''}
                        onChange={handleChange}
                        onKeyDown={handleKeyPress}
                    />
                    <label htmlFor="link">Enter link</label>
                </div>
            </div>
        </div>
    )
}