import React from 'react'
import {Routes, Route, Navigate} from 'react-router-dom'
import {LinksView} from './pages/LinksPage'
import {CreateView} from './pages/CreatePage'
import {DetailView} from './pages/DetailPage'
import {AuthView} from './pages/AuthPage'

export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Routes>
                <Route path="/links" exact element={<LinksView />} />
                <Route path="/create" exact element={<CreateView />} />
                <Route path="/detail/:id" element={<DetailView />} />
                <Route path="*" element={<Navigate to="/create" replace />} />
            </Routes>
        )
    }

    return (
        <Routes>
            <Route path="/" element={<AuthView />} />
            <Route path="*" element={<Navigate to="/" replace />} />
        </Routes>
    )
}
